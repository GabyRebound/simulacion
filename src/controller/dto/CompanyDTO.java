
package controller.dto;


public class CompanyDTO {
    private Double nroHectareas;
    private Double kilosPorHectarea;
    private Double sobreproduccionMin;
    private Double sobreproduccionMax;
    private Double fermentacionMin;
    private Double fermentacionMax;
    private Double filtradoMin;
    private Double filtradoMax;
    private Double demandaJovenesMin;
    private Double demandaJovenesModa;
    private Double demandaJovenesMax;
    private Double demandaAniejosMin;
    private Double demandaAniejosModa;
    private Double demandaAniejosMax;
    private Double precioJovenes;
    private Double precioEspumantes;
    private Double precioAniejos;

    public Double getNroHectareas() {
        return nroHectareas;
    }

    public Double getKilosPorHectarea() {
        return kilosPorHectarea;
    }

    public Double getSobreproduccionMin() {
        return sobreproduccionMin;
    }

    public Double getSobreproduccionMax() {
        return sobreproduccionMax;
    }

    public Double getFermentacionMin() {
        return fermentacionMin;
    }

    public Double getFermentacionMax() {
        return fermentacionMax;
    }

    public Double getFiltradoMin() {
        return filtradoMin;
    }

    public Double getFiltradoMax() {
        return filtradoMax;
    }

    public Double getDemandaJovenesMin() {
        return demandaJovenesMin;
    }

    public Double getDemandaJovenesModa() {
        return demandaJovenesModa;
    }

    public Double getDemandaJovenesMax() {
        return demandaJovenesMax;
    }

    public Double getDemandaAniejosMin() {
        return demandaAniejosMin;
    }

    public Double getDemandaAniejosModa() {
        return demandaAniejosModa;
    }

    public Double getDemandaAniejosMax() {
        return demandaAniejosMax;
    }

    public Double getPrecioJovenes() {
        return precioJovenes;
    }

    public Double getPrecioEspumantes() {
        return precioEspumantes;
    }

    public Double getPrecioAniejos() {
        return precioAniejos;
    }

    public void setNroHectareas(Double nroHectareas) {
        this.nroHectareas = nroHectareas;
    }

    public void setKilosPorHectarea(Double kilosPorHectarea) {
        this.kilosPorHectarea = kilosPorHectarea;
    }

    public void setSobreproduccionMin(Double sobreproduccionMin) {
        this.sobreproduccionMin = sobreproduccionMin;
    }

    public void setSobreproduccionMax(Double sobreproduccionMax) {
        this.sobreproduccionMax = sobreproduccionMax;
    }

    public void setFermentacionMin(Double fermentacionMin) {
        this.fermentacionMin = fermentacionMin;
    }

    public void setFermentacionMax(Double fermentacionMax) {
        this.fermentacionMax = fermentacionMax;
    }

    public void setFiltradoMin(Double filtradoMin) {
        this.filtradoMin = filtradoMin;
    }

    public void setFiltradoMax(Double filtradoMax) {
        this.filtradoMax = filtradoMax;
    }

    public void setDemandaJovenesMin(Double demandaJovenesMin) {
        this.demandaJovenesMin = demandaJovenesMin;
    }

    public void setDemandaJovenesModa(Double demandaJovenesModa) {
        this.demandaJovenesModa = demandaJovenesModa;
    }

    public void setDemandaJovenesMax(Double demandaJovenesMax) {
        this.demandaJovenesMax = demandaJovenesMax;
    }

    public void setDemandaAniejosMin(Double demandaAniejosMin) {
        this.demandaAniejosMin = demandaAniejosMin;
    }

    public void setDemandaAniejosModa(Double demandaAniejosModa) {
        this.demandaAniejosModa = demandaAniejosModa;
    }

    public void setDemandaAniejosMax(Double demandaAniejosMax) {
        this.demandaAniejosMax = demandaAniejosMax;
    }

    public void setPrecioJovenes(Double precioJovenes) {
        this.precioJovenes = precioJovenes;
    }

    public void setPrecioEspumantes(Double precioEspumantes) {
        this.precioEspumantes = precioEspumantes;
    }

    public void setPrecioAniejos(Double precioAniejos) {
        this.precioAniejos = precioAniejos;
    }
    
    public String toString() {
        String cad = "";
        cad += "nroHectareas  " + this.nroHectareas;
        cad += "kilosPorHectarea  " + this.kilosPorHectarea;
        cad += "sobreproduccionMin  " + this.sobreproduccionMin;
        cad += "sobreproduccionMax  " + this.sobreproduccionMax;
        cad += "fermentacionMin  " + this.fermentacionMin;
        cad += "fermentacionMax  " + this.fermentacionMax;
        cad += "filtradoMin  " + this.filtradoMin;
        cad += "filtradoMax  " + this.filtradoMax;
        cad += "demandaJovenesMin  " + this.demandaJovenesMin;
        cad += "demandaJovenesModa  " + this.demandaJovenesModa;
        cad += "demandaJovenesMax  " + this.demandaJovenesMax;
        cad += "demandaAniejosMin  " + this.demandaAniejosMin;
        cad += "demandaAniejosModa  " + this.demandaAniejosModa;
        cad += "demandaAniejosMax  " + this.demandaAniejosMax;
        cad += "precioJovenes  " + this.precioJovenes;
        cad += "precioEspumantes  " + this.precioEspumantes;
        cad += "precioAniejos  " + this.precioAniejos;
        return cad;
    }
}
