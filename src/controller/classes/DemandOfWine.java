package controller.classes;


public class DemandOfWine {

  DemandByRange demandSparkling;
  DemandTriangular demandYoung;
  DemandTriangular demandAged;
  double promedio;

  public double getPromedio() {
    double aux = (this.demandAged.getDemand() + this.demandSparkling.getDemand() + this.demandYoung.getDemand()) / 3;
    aux = Math.round(aux* 1000) / 1000d;
    return aux;
  }  

  public void setDemandSparkling(DemandByRange demandSparkling) {
    this.demandSparkling = demandSparkling;
  }

  public void setDemandYoung(DemandTriangular demandYoung) {
    this.demandYoung = demandYoung;
  }

  public void setDemandAged(DemandTriangular demandAged) {
    this.demandAged = demandAged;
  }

  public DemandByRange getDemandSparkling() { return demandSparkling; }

  public DemandTriangular getDemandYoung() {
    return demandYoung;
  }

  public DemandTriangular getDemandAged() {
    return demandAged;
  }

  @Override
  public String toString() {
    String cad = "";
    cad += "================ DemandOfWines===================\n";
    cad += "demandSparkling: "+ this.demandSparkling.toString()+ "\n";
    cad += "demandYoung: "+ this.demandYoung.toString()+ "\n";
    cad += "demandAged: "+ this.demandAged.toString()+ "\n";
    return cad;
  }

}
