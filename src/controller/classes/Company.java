package controller.classes;
import java.util.Comparator;
import java.lang.Comparable;

import controller.classes.Vineyard;
import controller.classes.Administration;

public class Company implements Comparator<Company>, Comparable<Company> {

  private Vineyard vineyard;
  private Administration administration;

  public Company( Vineyard vineyard, Administration administration ) {
    this.vineyard = vineyard;
    this.administration = administration;
  }

  public Vineyard getVineyard() {
    return this.vineyard;
  }

  public Administration getAdministration() {
    return administration;
  }

  // Overriding the compareTo method
  public int compareTo(Company company) {
    return (this.administration.getWinningWines()).compareTo(company.getAdministration().getWinningWines());
  }

  // Overriding the compare method to sort the age
  public int compare(Company company1, Company company2) {
    return (int) (company1.getAdministration().getWinningWines() - company2.getAdministration().getWinningWines());
  }

  public String toString() {
    String cad = "";
    cad += "================ Company ===================\n";
    cad += "vineyard "+ this.vineyard.toString()+ "\n";
    cad += "administration: "+ this.administration.toString()+ "\n";
    return cad;
  }

}