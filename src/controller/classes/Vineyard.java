package controller.classes;

import java.lang.Math;
import controller.classes.pruebas.CongruencialGenerator;
public class Vineyard {
  
  private double hectaresOfGrapes;
  private double kilogramsByHectareOfGrape;
  private double totalKilogramsOfGrapes;
  private double percentageOfOverproduction;        // modified
  private double percentageOfLostByTheSelectionProcess;
  private double percentageOfLostByTheMacerationProcess;    // modified
  private double kilogramsOfGrapeAfterSelection;
  private double litersOfWineAfterFermantation;             // modified 
  private double litersOfWineAfterMaceration;
  private double percentageDestinedForYoungWines;
  private double percentageDestinedForSparklingWines;
  private double percentageDestinedForAgedWines;
  private double litersOfYoungWine;
  private double litersOfSparklingWine;
  private double litersOfAgedWine;

    public double getHectaresOfGrapes() {
        return hectaresOfGrapes;
    }

    public double getKilogramsByHectareOfGrape() {
        return kilogramsByHectareOfGrape;
    }

    public double getPercentageOfOverproduction() {
        return percentageOfOverproduction;
    }

    public double getPercentageOfLostByTheSelectionProcess() {
        return percentageOfLostByTheSelectionProcess;
    }

    public double getPercentageOfLostByTheMacerationProcess() {
        return percentageOfLostByTheMacerationProcess;
    }

    public double getKilogramsOfGrapeAfterSelection() {
        return kilogramsOfGrapeAfterSelection;
    }

    public double getLitersOfWineAfterFermantation() {
        return litersOfWineAfterFermantation;
    }

    public double getLitersOfWineAfterMaceration() {
        return litersOfWineAfterMaceration;
    }

    public Double getOverproductionMin() {
        return overproductionMin;
    }

    public Double getOverproductionMax() {
        return overproductionMax;
    }

    public Double getFermantationMin() {
        return fermantationMin;
    }

    public Double getFermantationMax() {
        return fermantationMax;
    }

    public Double getMacerationFilterMin() {
        return macerationFilterMin;
    }

    public Double getMacerationFilterMax() {
        return macerationFilterMax;
    }

  public double getTotalKilogramsOfGrapes() {
    return totalKilogramsOfGrapes;
  }

  // ranges
  
  private Double overproductionMin;
  private Double overproductionMax;
  
  private Double fermantationMin;
  private Double fermantationMax;
  
  private Double macerationFilterMin;
  private Double macerationFilterMax;
  
  

  public Vineyard(Double hectaresOfGrapes, Double kilogramsByHectareOfGrape, Double percentageDestinedForSparklingWines, Double percentageDestinedForYoungWines, Double percentageDestinedForAgedWines, Double overproductionMin, Double overproductionMax, Double fermantationMin, Double fermantationMax, Double macerationFilterMin, Double macerationFilterMax) {
    this.hectaresOfGrapes = hectaresOfGrapes;
    this.kilogramsByHectareOfGrape = kilogramsByHectareOfGrape;
    this.percentageDestinedForSparklingWines = percentageDestinedForSparklingWines;
    this.percentageDestinedForYoungWines = percentageDestinedForYoungWines;
    this.percentageDestinedForAgedWines = percentageDestinedForAgedWines;
    
    this.overproductionMin = overproductionMin;
    this.overproductionMax = overproductionMax;

    this.fermantationMin = fermantationMin;
    this.fermantationMax = fermantationMax;

    this.macerationFilterMin = macerationFilterMin;
    this.macerationFilterMax = macerationFilterMax;
    
    this.generateAllPercentage();
    this.firstSimulation();
  }

  public double getLitersOfYoungWine() {
    return litersOfYoungWine;
  }

  public double getLitersOfSparklingWine() {
    return litersOfSparklingWine;
  }

  public double getLitersOfAgedWine() {
    return litersOfAgedWine;
  }

  private void generatePercentageOfOverProduction() {

    // Distribucion Uniforme continua
    Double limInf = this.overproductionMin;
    Double limSup = this.overproductionMax;

    this.percentageOfOverproduction = ((CongruencialGenerator.getInstance().nextRandomNumber() * (limSup-limInf))+limInf)*0.01;
    this.percentageOfOverproduction = Math.round(this.percentageOfOverproduction * 1000) / 1000d;

  }

  private void generatePercentageOfLostByTheSelectionProcess() {
    double valueRandom = CongruencialGenerator.getInstance().nextRandomNumber();
    if (valueRandom > 0 && valueRandom <= 0.15 ) {
      this.percentageOfLostByTheSelectionProcess = 0.01;
    } else if (valueRandom > 0.151 && valueRandom <= 0.45) {
      this.percentageOfLostByTheSelectionProcess = 0.02;
    } else if (valueRandom > 0.451 && valueRandom <= 0.7) {
      this.percentageOfLostByTheSelectionProcess = 0.035;
    } else if (valueRandom > 0.71 && valueRandom <= 0.85) {
      this.percentageOfLostByTheSelectionProcess = 0.05;
    } else if (valueRandom > 0.851 && valueRandom <= 0.95) {
      this.percentageOfLostByTheSelectionProcess = 0.07;
    } else if (valueRandom > 0.951 && valueRandom <= 1) {
      this.percentageOfLostByTheSelectionProcess = 0.1;
    }
    this.percentageOfLostByTheSelectionProcess = Math.round(this.percentageOfLostByTheSelectionProcess * 1000) / 1000d;
  }

    public double getPercentageDestinedForYoungWines() {
        return percentageDestinedForYoungWines;
    }

    public double getPercentageDestinedForSparklingWines() {
        return percentageDestinedForSparklingWines;
    }

    public double getPercentageDestinedForAgedWines() {
        return percentageDestinedForAgedWines;
    }

  private void generatePercentageOfLostByTheMacerationProcess() {
    // Distribucion Uniforme continua
    Double limInf = this.macerationFilterMin;
    Double limSup = this.macerationFilterMax;

    this.percentageOfLostByTheMacerationProcess = ((CongruencialGenerator.getInstance().nextRandomNumber() * (limSup-limInf))+limInf)*0.01;
    this.percentageOfLostByTheMacerationProcess = Math.round(this.percentageOfLostByTheMacerationProcess * 1000) / 1000d;
  }

  public void setHectaresOfGrapes(Integer hectaresOfGrapes) {
    this.hectaresOfGrapes = hectaresOfGrapes;
  }

  private void defineTotalKilogramsOfGrapes() {

    this.totalKilogramsOfGrapes = this.hectaresOfGrapes*this.kilogramsByHectareOfGrape;
    this.totalKilogramsOfGrapes += this.totalKilogramsOfGrapes*this.percentageOfOverproduction;
    this.totalKilogramsOfGrapes = Math.round(this.totalKilogramsOfGrapes * 1000) / 1000d;

  }

  private void defineKilogramsOfGrapeAfterSelection() {

    this.kilogramsOfGrapeAfterSelection = this.totalKilogramsOfGrapes - ( this.totalKilogramsOfGrapes * this.percentageOfLostByTheSelectionProcess );
    this.kilogramsOfGrapeAfterSelection = Math.round(this.kilogramsOfGrapeAfterSelection * 1000) / 1000d;
  }

  private void defineLitersOfWineAfterFermantation() {

    // Distribucion uniforme continua
    Double limInf = this.fermantationMin;
    Double limSup = this.fermantationMax;

    this.litersOfWineAfterFermantation = (this.kilogramsOfGrapeAfterSelection * ((CongruencialGenerator.getInstance().nextRandomNumber() * (limSup-limInf))+limInf))/1000;
    this.litersOfWineAfterFermantation = Math.round(this.litersOfWineAfterFermantation * 1000) / 1000d;
  }

  private void defineLitersOfWineAfterMaceration() {
    this.litersOfWineAfterMaceration = this.litersOfWineAfterFermantation * this.percentageOfLostByTheMacerationProcess;
    this.litersOfWineAfterMaceration = Math.round(this.litersOfWineAfterMaceration * 1000) / 1000d;
  }


  private void defineLitersOfYoungWine() {
    this.litersOfYoungWine = this.litersOfWineAfterMaceration * this.percentageDestinedForYoungWines;
    this.litersOfYoungWine = Math.round(this.litersOfYoungWine * 1000) / 1000d;
  }

  private void defineLitersOfSparklingWine() {
    this.litersOfSparklingWine = this.litersOfWineAfterMaceration * this.percentageDestinedForSparklingWines;
    this.litersOfSparklingWine = Math.round(this.litersOfSparklingWine * 1000) / 1000d;
  }

  private void defineLitersOfAgedWine() {
    this.litersOfAgedWine = this.litersOfWineAfterMaceration * this.percentageDestinedForAgedWines;
    this.litersOfAgedWine = Math.round(this.litersOfAgedWine * 1000) / 1000d;
  }

  public void generateAllPercentage() {
    this.generatePercentageOfOverProduction();
    this.generatePercentageOfLostByTheSelectionProcess();
    this.generatePercentageOfLostByTheMacerationProcess();
  }

  public void firstSimulation() {
    this.defineTotalKilogramsOfGrapes();
    this.defineKilogramsOfGrapeAfterSelection();
    this.defineLitersOfWineAfterFermantation();
    this.defineLitersOfWineAfterMaceration();
    this.defineLitersOfYoungWine();
    this.defineLitersOfSparklingWine();
    this.defineLitersOfAgedWine();
  }

  public String toString() {
    String cad = "===================================\n";
    cad += "hectaresOfGrapes: "+ this.hectaresOfGrapes + "\n";
    cad += "kilogramsByHectareOfGrape:" + this.kilogramsByHectareOfGrape + "\n";
    cad += "totalKilogramsOfGrapes:" + this.totalKilogramsOfGrapes + "\n";
    cad += "percentageOfOverproduction:" + this.percentageOfOverproduction + "\n";
    cad += "percentageOfLostByTheSelectionProcess:" + this.percentageOfLostByTheSelectionProcess + "\n";
    cad += "percentageOfLostByTheMacerationProcess:" + this.percentageOfLostByTheMacerationProcess + "\n";
    cad += "percentageDestinedForYoungWines:" + this.percentageDestinedForYoungWines + "\n";
    cad += "percentageDestinedForSparklingWines:" + this.percentageDestinedForSparklingWines + "\n";
    cad += "percentageDestinedForAgedWines:" + this.percentageDestinedForAgedWines + "\n";
    cad += "kilogramsOfGrapeAfterSelection:" + this.kilogramsOfGrapeAfterSelection + "\n";
    cad += "litersOfWineAfterFermantation:" + this.litersOfWineAfterFermantation + "\n";
    cad += "litersOfWineAfterMaceration:" + this.litersOfWineAfterMaceration+ "\n";
    cad += "litersOfYoungWine:" + this.litersOfYoungWine + "\n";
    cad += "litersOfSparklingWine:" + this.litersOfSparklingWine + "\n";
    cad += "litersOfAgedWine:" + this.litersOfAgedWine + "\n";
    return cad;
  }
}