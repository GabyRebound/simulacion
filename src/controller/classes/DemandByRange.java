package controller.classes;

import controller.classes.pruebas.CongruencialGenerator;

public class DemandByRange {

  private Double percentage;
  private Double demand;

  public Double getPercentage() {
    return percentage;
  }

  public Double getDemand() {
    return demand;
  }

  public DemandByRange() {
    this.generatePercentage();
    this.calculateDemand();
  }

  private void generatePercentage() {
    // calculate for ranges
    double valueRandom = CongruencialGenerator.getInstance().nextRandomNumber();
    if (valueRandom > 0 && valueRandom <= 0.2 ) {
      this.percentage = 0.7;
    } else if (valueRandom > 0.201 && valueRandom <= 0.7) {
      this.percentage = 0.8;
    } else if (valueRandom > 0.701 && valueRandom <= 1) {
      this.percentage = 0.9;
    }
    this.percentage = Math.round(this.percentage * 1000) / 1000d;
  }

  private void calculateDemand() {
    this.demand = this.percentage * 1000;
  }

  @Override
  public String toString() {
    String cad = "";
    cad += "================ Demand Ranges ===================\n";
    cad += "percentage: "+ this.percentage+ "\n";
    cad += "ranges : 0.7,  0.8,  0.9   \n";
    cad += "demand: "+ this.demand+ "\n";
    return cad;
  }

}
