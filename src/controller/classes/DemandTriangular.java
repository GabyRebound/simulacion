package controller.classes;
import java.lang.Math;
import controller.classes.pruebas.CongruencialGenerator;

public class DemandTriangular {


  private Double min;
  private Double moda;
  private Double max;
  private Double demand;

  public DemandTriangular(Double min, Double moda, Double max){
    this.min = min;
    this.moda = moda;
    this.max = max;
    this.calculateDemand();
  }

  private void calculateDemand() {

    // =SI(rand<=((moda-min)/(max-min));min+RAIZ(rand*(max-min)*(moda-min));max-RAIZ((1-rand)*(max-min)*(max-moda)))

    double rand = CongruencialGenerator.getInstance().nextRandomNumber();
    if (rand<=((moda-min)/(max-min))) {
      this.demand = this.min + Math.sqrt(rand*(this.max - this.min)*( this.moda - this.min));
    } else {
      this.demand = this.max - Math.sqrt((1-rand)*( this.max - this.min)*( this.max - this.moda));
    }
    this.demand = Math.round(this.demand* 1000) / 1000d;
  }

  public Double getMin() {
    return min;
  }

  public Double getModa() {
    return moda;
  }

  public Double getMax() {
    return max;
  }

  public Double getDemand() {
    return demand;
  }

  @Override
  public String toString() {
    String cad = "";
    cad += "================ Demand triangular ===================\n";
    cad += "min: "+ this.min+ "\n";
    cad += "moda: "+ this.moda+ "\n";
    cad += "max: "+ this.max+ "\n";
    cad += "demand: "+ this.demand+ "\n";
    return cad;
  }

}
