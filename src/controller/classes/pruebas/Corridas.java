
package controller.classes.pruebas;
import java.util.ArrayList;
import org.apache.commons.math3.distribution.NormalDistribution;

public class Corridas {
    
    final static double confianza = .95;
    static double alfa = 1-confianza;

    public static boolean pruebacorridas( double[] datos){
        //Creamos una lista para guardar los ceros y unos.
        ArrayList<Integer> bits = new ArrayList<>();
        int i, corridas, dato;
        double  media, varianza, z;
        //Revisa si cada dato actual es menor al dato anterior. 
        //Si es así, se guarda un 0, de lo contrario, se guarda un 1
        for (i=1; i<datos.length; i++){
            if (datos[i]<=datos[i-1]){
                bits.add(0);
            }
            else{
                bits.add(1);
            }                
        }

        //Imprimimos la cadena de ceros y unos
        for (i=0; i<bits.size(); i++){
            System.out.print(bits.get(i));
        }

        //Contamos las corridas. 
        corridas = 1;
        //Comenzamos observando el primer dígito
        dato= bits.get(0);
        //Comparamos cada dígito con el observado, cuando cambia es 
        //una nueva corrida
        for (i=1; i<bits.size(); i++){
            if (bits.get(i) != dato){
                corridas++;
                dato = bits.get(i);
            }
        }
        System.out.println("Corridas " + corridas);

        //Aplicamos las fórmulas para media, varianza y Z.
        media = (2*datos.length-1)/ (double)3;
        System.out.println("Media: " +media);        
        varianza = (16*datos.length-29)/(double) 90;
        System.out.println("Varianza: " + varianza);
        z= Math.abs((corridas-media)/Math.sqrt(varianza));
        System.out.println("Z=" + z);     

        //Obtenemos el valor Z de la tabla de distribución normal
        NormalDistribution normal = new NormalDistribution();
        double  zn =  normal.inverseCumulativeProbability(1-alfa/2);
        //Comparamos: si es mayor mi valor Z al de la tabla, no pasa
        return (z < zn);
        /*
        if (z < zn){
            System.out.println("No se rechaza que son independientes. " );
        }
        else{
            System.out.println("No Pasa la prueba de corridas");
        }
        */
    }
    /*
    public static void main(String[] args) {
        double[] datos = {
                         0.6129,
                         0.7097,
                         0.9355,
                         0.7742,
                         0.7419,
                         0.3226,
                         0.0323,
                         0.3871,
                         0.8710,
                         0.9677,
                         0.1613,
                         0,
                         1,
                         0.5806,
                         0.2903   
       };
       System.out.println(Corridas.pruebacorridas(datos));
    }
    */
}
