
package controller.classes.pruebas;

import java.math.*;
/**
 * @web http://jc-mouse.net/
 * @author Mouse
 */
public class CongruencialGenerator {
    
    private static CongruencialGenerator instance = null;

    // parametros del generador
    private BigInteger xn=new BigInteger("0"); //semilla 
    private final BigInteger a  = new BigInteger("1103515245"); //Multiplicador
    private final BigInteger m = new BigInteger("2").pow(32); //Modulo
    private final BigInteger c  = new BigInteger("12345"); //Constante aditiva
    private double[] valoresAleatorios;
    private int head = 0;

    private int precision = 5;//cantidad de digitos despues del punto decimal

    /** Constructor de clase */
    public CongruencialGenerator(){
        int cont = 0;
        double[] serie = new double[100000];
        do {
            for(int i=0; i<100000; i++) {
                serie[i] = Double.parseDouble(this.next().toString());
            }
        // hacemos pruebas de independencia(Corridas de arriba y abajo) y uniformidad(Kolmogorov)
        } while(!Kolmogorov.isKolmogorov(serie) && !Corridas.pruebacorridas(serie));
        this.valoresAleatorios = serie;
    }
    
    public static CongruencialGenerator getInstance() {
        if (CongruencialGenerator.instance == null) {
            CongruencialGenerator.instance = new CongruencialGenerator();
        }
        return CongruencialGenerator.instance;
    }
    
    public double nextRandomNumber() {
        double aux = this.valoresAleatorios[this.head];
        this.head++;
        return aux;
    }

    public BigInteger getXn() {
        return xn;
    }

    public void setXn(BigInteger xn) {
        this.xn = xn;
    }

    public int getPrecision() {
        return precision;
    }

    public void setPrecision(int precision) {
        this.precision = precision;
    }   

    /**
 * Metodo que obtiene un numero aleatorio entre 0 y 1
 * 
 * @param No existe parametros de entrada
 * @return El numero aleatorio estandar 
 */
    private BigDecimal next()
    {           
        //xn = (a * xn + c) % m ;
        xn = ((xn.multiply(a)).add(c)).mod(m);        
        BigDecimal x  = new BigDecimal( xn.floatValue() / m.floatValue() ).setScale( precision, BigDecimal.ROUND_HALF_UP);        
        return  x;
    }
    
    
    public static void main(String[] args) {
        for(int i = 0; i<10000; i++) {
            System.out.println(CongruencialGenerator.getInstance().nextRandomNumber());
        }
    }
    
}