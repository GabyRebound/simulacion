package controller.classes;

public class SaleOfWines {
  private Double priceByYoungWine;
  private Double priceBySparklingWine;
  private Double priceByAgedWine;

  public Double getPriceByYoungWine() {
    return priceByYoungWine;
  }

  public Double getPriceBySparklingWine() {
    return priceBySparklingWine;
  }

  public Double getPriceByAgedWine() {
    return priceByAgedWine;
  }

  public void setPriceByYoungWine(Double priceByYoungWine) {
    this.priceByYoungWine = priceByYoungWine;
  }

  public void setPriceBySparklingWine(Double priceBySparklingWine) {
    this.priceBySparklingWine = priceBySparklingWine;
  }

  public void setPriceByAgedWine(Double priceByAgedWine) {
    this.priceByAgedWine = priceByAgedWine;
  }


  @Override
  public String toString() {
    String cad = "";
    cad += "================ SalesOfWines ===================\n";
    cad += "priceByYoungWine: "+ this.priceByYoungWine+ "\n";
    cad += "priceBySparklingWine: "+ this.priceBySparklingWine+ "\n";
    cad += "priceByAgedWine: "+ this.priceByAgedWine+ "\n";
    return cad;
  }
}
