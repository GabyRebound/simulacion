

package controller.classes;

public class UnsatisfiedDemandWine {
  private Double unsatisfiedDemandWinesYoung;
  private Double unsatisfiedDemandWinesSparkling;
  private Double unsatisfiedDemandWinesAge;
  private Vineyard vineyard;
  private DemandOfWine demandOfWine;
  private double promedio;

    public double getPromedio() {
        double aux = (this.unsatisfiedDemandWinesYoung + this.unsatisfiedDemandWinesSparkling + this.unsatisfiedDemandWinesAge) / 3;
        aux = Math.round(aux* 1000) / 1000d;
        return aux;
    }
  
    public Double getUnsatisfiedDemandWinesYoung() {
        return unsatisfiedDemandWinesYoung;
    }

    public Double getUnsatisfiedDemandWinesSparkling() {
        return unsatisfiedDemandWinesSparkling;
    }

    public Double getUnsatisfiedDemandWinesAge() {
        return unsatisfiedDemandWinesAge;
    }
  

  public UnsatisfiedDemandWine(Vineyard vineyard, DemandOfWine demandOfWine) {
    this.vineyard = vineyard;
    this.demandOfWine = demandOfWine;

    this.generateUnsatisfiedDemandWinesYoung();
    this.generateUnsatisfiedDemandSparkling();
    this.generateUnsatisfiedDemandWinesAge();
  }

  private void generateUnsatisfiedDemandWinesYoung() {
    Double litersOfYoungWine =  this.vineyard.getLitersOfYoungWine();
    Double demandYoung = this.demandOfWine.getDemandYoung().getDemand();

    if(demandYoung > litersOfYoungWine) {
      this.unsatisfiedDemandWinesYoung = demandYoung - litersOfYoungWine;
    } else {
      this.unsatisfiedDemandWinesYoung =  0.0;
    }
    this.unsatisfiedDemandWinesYoung = Math.round(this.unsatisfiedDemandWinesYoung * 1000) / 1000d;
  }

  private void generateUnsatisfiedDemandSparkling() {
    Double litersOfSparklingWine =  this.vineyard.getLitersOfSparklingWine();
    Double demandSparkling = this.demandOfWine.getDemandSparkling().getDemand();

    if(demandSparkling>litersOfSparklingWine) {
      this.unsatisfiedDemandWinesSparkling = demandSparkling - litersOfSparklingWine;
    } else {
      this.unsatisfiedDemandWinesSparkling =  0.0;
    }

    this.unsatisfiedDemandWinesSparkling = Math.round(this.unsatisfiedDemandWinesSparkling * 1000) / 1000d;
  }

  private void generateUnsatisfiedDemandWinesAge() {
    Double litersOfAgeWine =  this.vineyard.getLitersOfAgedWine();
    Double demandAge = this.demandOfWine.getDemandAged().getDemand();


    if(demandAge > litersOfAgeWine) {
      this.unsatisfiedDemandWinesAge = demandAge - litersOfAgeWine;
    } else {
      this.unsatisfiedDemandWinesAge =  0.0;
    }
    this.unsatisfiedDemandWinesAge = Math.round(this.unsatisfiedDemandWinesAge * 1000) / 1000d;
  }

  @Override
  public String toString() {
    String cad = "";
    cad += "================ LossOfWines ===================\n";
    cad += "unsatisfiedDemandWinesYoung: "+ this.unsatisfiedDemandWinesYoung + "\n";
    cad += "unsatisfiedDemandWinesSparkling: "+ this.unsatisfiedDemandWinesSparkling + "\n";
    cad += "unsatisfiedDemandWinesAge: "+ this.unsatisfiedDemandWinesAge + "\n";
    return cad;
  }
}
