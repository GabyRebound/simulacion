package controller.classes;

public class AdministrativeExpenses {

  private Double fixedCosts;
  private Double variablesCostsByYoungWine;
  private Double variablesCostsBySparklingWine;
  private Double variablesCostsByAgedWine;

  public Double getFixedCosts() {
    return fixedCosts;
  }

  public Double getVariablesCostsByYoungWine() {
    return variablesCostsByYoungWine;
  }

  public Double getVariablesCostsBySparklingWine() {
    return variablesCostsBySparklingWine;
  }

  public Double getVariablesCostsByAgedWine() {
    return variablesCostsByAgedWine;
  }

  public void setFixedCosts(Double fixedCosts) {
    this.fixedCosts = fixedCosts;
  }

  public void setVariablesCostsByYoungWine(Double variablesCostsByYoungWine) {
    this.variablesCostsByYoungWine = variablesCostsByYoungWine;
  }

  public void setVariablesCostsBySparklingWine(Double variablesCostsBySparklingWine) {
    this.variablesCostsBySparklingWine = variablesCostsBySparklingWine;
  }

  public void setVariablesCostsByAgedWine(Double variablesCostsByAgedWine) {
    this.variablesCostsByAgedWine = variablesCostsByAgedWine;
  }

  @Override
  public String toString() {
    String cad = "";
    cad += "================ AdministrativeExpenses ===================\n";
    cad += "fixedCosts "+ this.fixedCosts+ "\n";
    cad += "variablesCostsByYoungWine: "+ this.variablesCostsByYoungWine+ "\n";
    cad += "variablesCostsBySparklingWine: "+ this.variablesCostsBySparklingWine+ "\n";
    cad += "variablesCostsByAgedWine: "+ this.variablesCostsByAgedWine+ "\n";
    return cad;
  }
}
