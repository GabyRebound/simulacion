package controller.classes;


public class Administration {

  private Vineyard vineyard;
  private AdministrativeExpenses administrativeExpenses;
  private SaleOfWines saleOfWines;
  private DemandOfWine demandOfWine;
  private Double winningWines;
  private LossOfWines lossOfWines;
  private UnsatisfiedDemandWine unsatisfiedDemandWine;

    public AdministrativeExpenses getAdministrativeExpenses() {
        return administrativeExpenses;
    }

    public SaleOfWines getSaleOfWines() {
        return saleOfWines;
    }

    public DemandOfWine getDemandOfWine() {
        return demandOfWine;
    }

    public LossOfWines getLossOfWines() {
        return lossOfWines;
    }

    public Double getTotalUtility() {
        return totalUtility;
    }

    public UnsatisfiedDemandWine getUnsatisfiedDemandWine() {
        return unsatisfiedDemandWine;
    }
  private Double totalUtility;

  public Administration(AdministrativeExpenses administrativeExpenses, SaleOfWines saleOfWines, DemandOfWine demandOfWine, Vineyard vineyard) {
    this.administrativeExpenses = administrativeExpenses;
    this.saleOfWines = saleOfWines;
    this.demandOfWine = demandOfWine;
    this.vineyard = vineyard;
    this.generateWinningWines();
    this.lossOfWines = new LossOfWines(this.vineyard, this.demandOfWine, this.saleOfWines);
    this.unsatisfiedDemandWine = new UnsatisfiedDemandWine(this.vineyard, this.demandOfWine);
    this.generateTotalUtility();
  }

  public Double getWinningWines() {
    this.generateWinningWines();
    return winningWines;
  }

  private void generateTotalUtility() {
    this.totalUtility = this.winningWines - (this.lossOfWines.getLossOfWinesYoung() + this.lossOfWines.getLossOfWinesSparkling() + this.lossOfWines.getLossOfWinesAge());
  }

  private Double generateWinningWines() {
    this.winningWines = 0.0;
    this.winningWines += this.demandOfWine.getDemandYoung().getDemand() * this.saleOfWines.getPriceByYoungWine();
    this.winningWines += this.demandOfWine.getDemandSparkling().getDemand() * this.saleOfWines.getPriceBySparklingWine();
    this.winningWines += this.demandOfWine.getDemandAged().getDemand() * this.saleOfWines.getPriceByAgedWine();

    this.winningWines  = Math.round(this.winningWines * 1000) / 1000d;
    return this.winningWines;
  }


  public void setAdministrativeExpenses(AdministrativeExpenses administrativeExpenses) {
    this.administrativeExpenses = administrativeExpenses;
  }

  public void setSaleOfWines(SaleOfWines saleOfWines) {
    this.saleOfWines = saleOfWines;
  }

  public void setDemandOfWine(DemandOfWine demandOfWine) {
    this.demandOfWine = demandOfWine;
  }

  @Override
  public String toString() {
    String cad = "";
    cad += "================ Administration ===================\n";
    cad += "administrativeExpenses: "+ this.administrativeExpenses.toString()+ "\n";
    cad += "saleOfWines: "+ this.saleOfWines.toString()+ "\n";
    cad += "demandOfWine: "+ this.demandOfWine.toString()+ "\n";
    cad += "winningWines: "+ this.winningWines+ "\n";
    cad += "lossOfWines: "+ this.lossOfWines+ "\n";
    cad += "unsatisfiedDemandWine: "+ this.unsatisfiedDemandWine+ "\n";
    cad += "totalUtility: "+ this.totalUtility+ "\n";
    return cad;
  }

}
