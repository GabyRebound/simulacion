package controller.classes;

public class LossOfWines {
  private Double lossOfWinesYoung;
  private Double lossOfWinesSparkling;
  private Double lossOfWinesAge;
  private Vineyard vineyard;
  private DemandOfWine demandOfWine;

  public Double getLossOfWinesYoung() {
    return lossOfWinesYoung;
  }

  public Double getLossOfWinesSparkling() {
    return lossOfWinesSparkling;
  }

  public Double getLossOfWinesAge() {
    return lossOfWinesAge;
  }

  private SaleOfWines saleOfWines;

  public LossOfWines(Vineyard vineyard, DemandOfWine demandOfWine, SaleOfWines saleOfWines) {
    this.vineyard = vineyard;
    this.demandOfWine = demandOfWine;
    this.saleOfWines = saleOfWines;
    this.generateLossOfWinesYoung();
    this.generateLossOfWinesSparkling();
    this.generateLossOfWinesAge();
  }

  private void generateLossOfWinesYoung() {
    Double litersOfYoungWine =  this.vineyard.getLitersOfYoungWine();
    Double demandYoung = this.demandOfWine.getDemandYoung().getDemand();
    Double priceOfWineYoung = saleOfWines.getPriceByYoungWine();


    if(demandYoung<litersOfYoungWine) {
      this.lossOfWinesYoung = (demandYoung - litersOfYoungWine )*priceOfWineYoung;
    } else {
      this.lossOfWinesYoung =  0.0;
    }
    this.lossOfWinesYoung = Math.round(this.lossOfWinesYoung* 1000) / 1000d;
  }

  private void generateLossOfWinesSparkling() {
    Double litersOfSparklingWine =  this.vineyard.getLitersOfSparklingWine();
    Double demandSparkling = this.demandOfWine.getDemandSparkling().getDemand();
    Double priceOfWineSparkling = saleOfWines.getPriceBySparklingWine();

    if(demandSparkling<litersOfSparklingWine) {
      this.lossOfWinesSparkling = (litersOfSparklingWine - demandSparkling) * priceOfWineSparkling;
    } else {
      this.lossOfWinesSparkling =  0.0;
    }

    this.lossOfWinesSparkling = Math.round(this.lossOfWinesSparkling* 1000) / 1000d;
  }

  private void generateLossOfWinesAge() {
    Double litersOfAgeWine =  this.vineyard.getLitersOfAgedWine();
    Double demandAge = this.demandOfWine.getDemandAged().getDemand();
    Double priceOfWineAge = saleOfWines.getPriceByAgedWine();


    if(demandAge<litersOfAgeWine) {
      this.lossOfWinesAge = (litersOfAgeWine - demandAge)*priceOfWineAge;
    } else {
      this.lossOfWinesAge =  0.0;
    }
    this.lossOfWinesAge = Math.round(this.lossOfWinesAge* 1000) / 1000d;
  }

  @Override
  public String toString() {
    String cad = "";
    cad += "================ LossOfWines ===================\n";
    cad += "lossOfWinesYoung: "+ this.lossOfWinesYoung+ "\n";
    cad += "lossOfWinesSparkling: "+ this.lossOfWinesSparkling+ "\n";
    cad += "lossOfWinesAge: "+ this.lossOfWinesAge+ "\n";
    return cad;
  }

}
