
package view;
import controller.dto.CompanyDTO;
import java.util.ArrayList;
import java.util.Collections;
import controller.classes.*;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;



public class Ventana4 extends javax.swing.JDialog {

    private CompanyDTO companyDTO;
    private Company bestCompany;
    private Map<String, ArrayList<Company> > mapAllSimlation = new HashMap<String, ArrayList<Company>>();
    
    Map<Double, Map<String, Double>> mapCompanyData = new HashMap<Double, Map<String, Double>>();
    Map<Double, Map<String, Company>> mapCompany = new HashMap<Double, Map<String, Company>>();
    public Ventana4(java.awt.Frame parent, boolean modal, CompanyDTO companyDTO) {
        super(parent, modal);
        this.companyDTO = companyDTO;
        initComponents();
        sumulation(0.1, 0.8 , 0.1);
        sumulation(0.2, 0.7 , 0.1);
        sumulation(0.3, 0.6 , 0.1);
        sumulation(0.4, 0.5 , 0.1);
        sumulation(0.5, 0.4 , 0.1);
        sumulation(0.6, 0.3 , 0.1);
        sumulation(0.7, 0.2 , 0.1);
        sumulation(0.8, 0.1 , 0.1);
        chooseBestCompany();
        initComponents2();
        
        
        this.demanda.setText(this.bestCompany.getAdministration().getUnsatisfiedDemandWine().getPromedio()+"");
        demandaInsatisfecha();
    }
    
    private void demandaInsatisfecha() {
        
        double menor= -1 ;
        for (Map.Entry<String, ArrayList<Company>> dataSimulation : mapAllSimlation.entrySet()){
            String claveSim = dataSimulation.getKey();
            ArrayList<Company> valueSim = dataSimulation.getValue();
            double cont = 0, prom =0;
            for(Company company: valueSim) {
                cont += company.getAdministration().getUnsatisfiedDemandWine().getPromedio();
            }
            prom = cont/valueSim.size();
            if(menor == -1) {
                menor = prom;
            }
            if(prom<=menor) {
                this.insatisfechaPromedio.setText(prom+"");
                String[] demand = claveSim.split("-");
                this.InsatisfechaJovenes.setText(demand[0]);
                this.InsatisfechaAniejos.setText(demand[1]);
                this.InsatisfechaEspumantes.setText(demand[2]);
            }
        }
        
    }
    
    private void initComponents2() {
        this.jPanel1.setOpaque(false);
        this.jPanel2.setOpaque(false);
    }
    
    private void chooseBestCompany() {
        System.out.println("entro");
        Double key = 0.0;
        
        for (Map.Entry<Double, Map<String, Double>> datacompany : mapCompanyData.entrySet()){
            Double clave = datacompany.getKey();
            if(clave > key) {
                key = clave;
            }
        }
        System.out.println(key.toString());
        this.gananciaMaxima.setText(key.toString());
        
        this.destinacionJovenes.setText(""+(int) (this.mapCompanyData.get(key).get("jovenes")*100));
        this.destinacionAniejos.setText(""+(int) (this.mapCompanyData.get(key).get("aniejos")*100));
        this.destinacionEspumantes.setText(""+(int) (this.mapCompanyData.get(key).get("espumantes")*100));
        
        this.bestCompany = this.mapCompany.get(key).get("best");
    }
    
    private void sumulation(Double destinationYoung, Double destinationAge, Double destinationSparkling) {
        Double nroHectareas = this.companyDTO.getNroHectareas();
        Double kilosPorHectarea = this.companyDTO.getKilosPorHectarea();
        Double sobreproduccionMin = this.companyDTO.getSobreproduccionMin();
        Double sobreproduccionMax = this.companyDTO.getSobreproduccionMax();
        Double fermentacionMin = this.companyDTO.getFermentacionMin();
        Double fermentacionMax = this.companyDTO.getFermentacionMax();        
        Double filtradoMin = this.companyDTO.getFiltradoMin();
        Double filtradoMax = this.companyDTO.getFiltradoMax();
        Double demandaJovenesMin = this.companyDTO.getDemandaJovenesMin();        
        Double demandaJovenesModa = this.companyDTO.getDemandaJovenesModa();
        Double demandaJovenesMax = this.companyDTO.getDemandaJovenesMax();
        Double demandaAniejosMin = this.companyDTO.getDemandaAniejosMin();
        Double demandaAniejosModa = this.companyDTO.getDemandaAniejosModa();
        Double demandaAniejosMax = this.companyDTO.getDemandaAniejosMax();
        Double precioJovenes = this.companyDTO.getPrecioJovenes();
        Double precioEspumantes = this.companyDTO.getPrecioEspumantes();
        Double precioAniejos = this.companyDTO.getPrecioAniejos();
        
        
   
        
        // =======================================================
        // empezamos a reemplazar datos para la simulacion
        // =======================================================
        Double hectaresOfGrapes = nroHectareas;
        Double kilogramsByHectaresOfGrape = kilosPorHectarea;
        Double percentageDestinedForSparkilngWines = destinationSparkling;
        Double percentageDestinedForYoungWines = destinationYoung;
        Double percentageDestinedForAgesWines = destinationAge;
        
        Double overproductionMin = sobreproduccionMin;
        Double overproductionMax = sobreproduccionMax;
        Double fermantationMin = fermentacionMin;
        Double fermantationMax = fermentacionMax ;
        Double macerationFilterMin = filtradoMin;
        Double macerationFilterMax = filtradoMax;

        // Administration  HARDCODE
        Double fixedCost =  15600.0;
        Double variablesCostsByYoungWine = 2.1;
        Double variablesCostsByAgeWine = 7.1;
        Double variablesCostsBySparklingWine = 3.1;

        // SaleOfWine
        Double priceByYoungWine = precioJovenes;
        Double priceBySparklingWine = precioEspumantes;
        Double priceByAgeWine = precioAniejos;

        // demand
        Double demandYoungMin = demandaJovenesMin;
        Double demandYoungModa = demandaJovenesModa;
        Double demandYoungMax = demandaJovenesMax;

        Double demandAgeMin = demandaAniejosMin;
        Double demandAgeModa = demandaAniejosModa;
        Double demandAgeMax = demandaAniejosMax;
        
        ArrayList<Company> arrayCompany = new ArrayList<Company>();
        Double promedio = 0.0;
        
        for(int i=0; i< 100; i++) {
          try {
            // ==========================================
            // created and set the datas for the vineyard
            // ==========================================

            Vineyard vineyard = new Vineyard(hectaresOfGrapes, kilogramsByHectaresOfGrape, percentageDestinedForSparkilngWines, percentageDestinedForYoungWines, percentageDestinedForAgesWines, overproductionMin, overproductionMax, fermantationMin, fermantationMax, macerationFilterMin, macerationFilterMax);

            // ==========================================================
            // created and set the datas for the Company's Administration
            // ==========================================================

            AdministrativeExpenses administrativeExpenses = new AdministrativeExpenses();
            administrativeExpenses.setFixedCosts(fixedCost);
            administrativeExpenses.setVariablesCostsByYoungWine(variablesCostsByYoungWine);
            administrativeExpenses.setVariablesCostsByAgedWine(variablesCostsByAgeWine);
            administrativeExpenses.setVariablesCostsBySparklingWine(variablesCostsBySparklingWine);

            SaleOfWines saleOfWines = new SaleOfWines();
            saleOfWines.setPriceByYoungWine(priceByYoungWine);
            saleOfWines.setPriceBySparklingWine(priceBySparklingWine);
            saleOfWines.setPriceByAgedWine(priceByAgeWine);

            DemandOfWine demandOfWine = new DemandOfWine();
            demandOfWine.setDemandYoung(new DemandTriangular(demandYoungMin, demandYoungModa, demandYoungMax));
            demandOfWine.setDemandAged(new DemandTriangular(demandAgeMin, demandAgeModa, demandAgeMax));
            demandOfWine.setDemandSparkling(new DemandByRange());

            Administration administration = new Administration(administrativeExpenses, saleOfWines, demandOfWine, vineyard);

            // =======================================================================
            // created the company with the objects required(vineyard, administration)
            // =======================================================================

            Company company = new Company(vineyard, administration);
            promedio += company.getAdministration().getWinningWines();
            arrayCompany.add(company);
            
          } catch (Exception e) {
            i--;
          }
        }
        
        Collections.sort(arrayCompany);
        
        // promedio general para una simulacion
        promedio = promedio/arrayCompany.size();
        promedio = Math.round(promedio * 1000) / 1000d;
        
        Company bestCompany = arrayCompany.get(arrayCompany.size()-1);
        Company worstCompany = arrayCompany.get(0);
        
        String mensaje = "";
        mensaje += "[ jovenes: " + destinationYoung.toString();
        mensaje += ", añejos: " + destinationAge.toString();
        mensaje += ", espumantes : " + destinationSparkling.toString()+" ]";
        mensaje += ", promedio: " + promedio;
        mensaje += ", mejor: " + bestCompany.getAdministration().getWinningWines();
        mensaje += ", peor: " + worstCompany.getAdministration().getWinningWines();
        
        // agregando al mapa de datos de los promedios de las companias
        
        HashMap<String, Double> destinaciones = new HashMap<String, Double>();
        destinaciones.put("jovenes", destinationYoung);
        destinaciones.put("aniejos", destinationAge);
        destinaciones.put("espumantes", destinationSparkling);
        
        this.mapCompanyData.put(promedio, destinaciones);
        
        // agregando al mapa de las mejores y peores companias
        HashMap<String, Company> companies = new HashMap<String, Company>();
        companies.put("best", bestCompany);
        companies.put("worst", worstCompany);
        this.mapCompany.put(promedio, companies);
        
        String des = (int) (destinationYoung*100) + "-" + (int) (destinationAge*100) +"-" + (int)(destinationSparkling*100);
        
        this.mapAllSimlation.put( des , arrayCompany);
        
        // System.out.println(mensaje);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        gananciaMaxima = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        InsatisfechaJovenes = new javax.swing.JLabel();
        InsatisfechaEspumantes = new javax.swing.JLabel();
        InsatisfechaAniejos = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        back = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        demanda = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        insatisfechaPromedio = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        destinacionJovenes = new javax.swing.JLabel();
        destinacionEspumantes = new javax.swing.JLabel();
        destinacionAniejos = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(1000, 700));
        setMinimumSize(new java.awt.Dimension(1000, 700));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Dialog", 3, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("SIMULADOR DE PRODUCCION DE VINOS");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(276, 30, 539, 40));

        jLabel1.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Si usted desa obtener la ganancia maxima de :");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(53, 132, -1, -1));

        gananciaMaxima.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        gananciaMaxima.setForeground(new java.awt.Color(0, 0, 0));
        gananciaMaxima.setText("------------");
        getContentPane().add(gananciaMaxima, new org.netbeans.lib.awtextra.AbsoluteConstraints(449, 132, 110, -1));

        jLabel4.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("Usted debe asignar los siguientes porcentajes de distribucion a los 3 tipos de vino de la siguiente manera:");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(53, 161, -1, -1));

        jLabel5.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setText("Jovenes");

        jLabel6.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setText("Espumantes");

        jLabel7.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 0, 0));
        jLabel7.setText("Añejos");

        InsatisfechaJovenes.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        InsatisfechaJovenes.setForeground(new java.awt.Color(0, 0, 0));
        InsatisfechaJovenes.setText("102");

        InsatisfechaEspumantes.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        InsatisfechaEspumantes.setForeground(new java.awt.Color(0, 0, 0));
        InsatisfechaEspumantes.setText("202");

        InsatisfechaAniejos.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        InsatisfechaAniejos.setForeground(new java.awt.Color(0, 0, 0));
        InsatisfechaAniejos.setText("702");

        jLabel12.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 0, 0));
        jLabel12.setText("%");

        jLabel13.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(0, 0, 0));
        jLabel13.setText("%");

        jLabel14.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(0, 0, 0));
        jLabel14.setText("%");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(jLabel5)
                .addGap(41, 41, 41)
                .addComponent(InsatisfechaJovenes)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel12)
                .addGap(65, 65, 65)
                .addComponent(jLabel6)
                .addGap(26, 26, 26)
                .addComponent(InsatisfechaEspumantes)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel13)
                .addGap(43, 43, 43)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(InsatisfechaAniejos)
                .addGap(18, 18, 18)
                .addComponent(jLabel14)
                .addContainerGap(112, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(InsatisfechaJovenes)
                            .addComponent(jLabel12)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(InsatisfechaEspumantes)
                            .addComponent(jLabel13)
                            .addComponent(jLabel7)
                            .addComponent(InsatisfechaAniejos)
                            .addComponent(jLabel14))))
                .addGap(0, 33, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(131, 444, -1, -1));

        jLabel11.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 0, 0));
        jLabel11.setText("$U$");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 130, -1, -1));

        back.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/atras_v.jpeg"))); // NOI18N
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });
        getContentPane().add(back, new org.netbeans.lib.awtextra.AbsoluteConstraints(83, 574, 160, 48));

        jLabel3.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("Demanda Insatisfecha");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(265, 264, 184, 37));

        jLabel15.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(0, 0, 0));
        jLabel15.setText("Ltrs");
        getContentPane().add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(534, 274, -1, -1));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/graficas_v.jpeg"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(368, 574, 149, 48));

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ver_detalle_v.jpeg"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(675, 574, 174, 48));

        demanda.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        demanda.setText("-----");
        getContentPane().add(demanda, new org.netbeans.lib.awtextra.AbsoluteConstraints(469, 274, -1, -1));

        jLabel18.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel18.setText("Si usted desea tener la minima demanda insatisfecha  de: ");
        getContentPane().add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(53, 362, -1, -1));

        insatisfechaPromedio.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        insatisfechaPromedio.setText("-----------");
        getContentPane().add(insatisfechaPromedio, new org.netbeans.lib.awtextra.AbsoluteConstraints(527, 362, -1, -1));

        jLabel20.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(0, 0, 0));
        jLabel20.setText("Ltrs");
        getContentPane().add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(663, 362, -1, -1));

        jLabel21.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel21.setText("Debe distribuir de la siguiente manera");
        getContentPane().add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(53, 385, -1, -1));

        jLabel22.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(0, 0, 0));
        jLabel22.setText("Jovenes");

        jLabel23.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(0, 0, 0));
        jLabel23.setText("Espumantes");

        jLabel24.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(0, 0, 0));
        jLabel24.setText("Añejos");

        destinacionJovenes.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        destinacionJovenes.setForeground(new java.awt.Color(0, 0, 0));
        destinacionJovenes.setText("102");

        destinacionEspumantes.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        destinacionEspumantes.setForeground(new java.awt.Color(0, 0, 0));
        destinacionEspumantes.setText("202");

        destinacionAniejos.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        destinacionAniejos.setForeground(new java.awt.Color(0, 0, 0));
        destinacionAniejos.setText("70 2");

        jLabel25.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(0, 0, 0));
        jLabel25.setText("%");

        jLabel26.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(0, 0, 0));
        jLabel26.setText("%");

        jLabel27.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(0, 0, 0));
        jLabel27.setText("%");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(jLabel22)
                .addGap(41, 41, 41)
                .addComponent(destinacionJovenes)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel25)
                .addGap(65, 65, 65)
                .addComponent(jLabel23)
                .addGap(26, 26, 26)
                .addComponent(destinacionEspumantes)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel26)
                .addGap(61, 61, 61)
                .addComponent(jLabel24)
                .addGap(18, 18, 18)
                .addComponent(destinacionAniejos)
                .addGap(12, 12, 12)
                .addComponent(jLabel27)
                .addContainerGap(88, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(destinacionJovenes)
                    .addComponent(jLabel25)
                    .addComponent(jLabel22)
                    .addComponent(jLabel23)
                    .addComponent(destinacionEspumantes)
                    .addComponent(jLabel26)
                    .addComponent(jLabel24)
                    .addComponent(destinacionAniejos)
                    .addComponent(jLabel27))
                .addGap(0, 36, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(126, 196, -1, -1));

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/frontis vino 2.jpg"))); // NOI18N
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 700));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_backActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        Graficas dialog = new Graficas(new javax.swing.JFrame(), true, this.bestCompany, this.mapCompany);
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        VentanasPestañas dialog = new VentanasPestañas(new javax.swing.JFrame(), true, this.mapAllSimlation);
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel InsatisfechaAniejos;
    private javax.swing.JLabel InsatisfechaEspumantes;
    private javax.swing.JLabel InsatisfechaJovenes;
    private javax.swing.JButton back;
    private javax.swing.JLabel demanda;
    private javax.swing.JLabel destinacionAniejos;
    private javax.swing.JLabel destinacionEspumantes;
    private javax.swing.JLabel destinacionJovenes;
    private javax.swing.JLabel gananciaMaxima;
    private javax.swing.JLabel insatisfechaPromedio;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables
}
