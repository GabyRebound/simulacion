/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;
import java.util.Map;
import java.util.ArrayList;
import controller.classes.*;
import javax.swing.table.TableColumnModel;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author alex
 */
public class VentanasPestañas extends javax.swing.JDialog {

    /**
     * Creates new form VentanasPestañas
     */
    
    private Map<String, ArrayList<Company> > mapAllSimlation;
    
    public VentanasPestañas(java.awt.Frame parent, boolean modal, Map<String, ArrayList<Company> > mapAllSimlation) {
        super(parent, modal);
        this.mapAllSimlation = mapAllSimlation;
        initComponents();
        loadTable((DefaultTableModel) this.jTable1.getModel(), "10-80-10", this.mapAllSimlation.get("10-80-10"));
        loadTable((DefaultTableModel) this.jTable2.getModel(), "20-70-10", this.mapAllSimlation.get("20-70-10"));
        loadTable((DefaultTableModel) this.jTable3.getModel(), "30-60-10", this.mapAllSimlation.get("30-60-10"));
        loadTable((DefaultTableModel) this.jTable4.getModel(), "40-50-10", this.mapAllSimlation.get("40-50-10"));
        loadTable((DefaultTableModel) this.jTable5.getModel(), "50-40-10", this.mapAllSimlation.get("50-40-10"));
        loadTable((DefaultTableModel) this.jTable6.getModel(), "60-30-10", this.mapAllSimlation.get("60-30-10"));
        loadTable((DefaultTableModel) this.jTable7.getModel(), "70-20-10", this.mapAllSimlation.get("70-20-10"));
        loadTable((DefaultTableModel) this.jTable8.getModel(), "80-10-10", this.mapAllSimlation.get("80-10-10"));
        
        tablaGenenal();
        // implementar un metodo que reciba una tabla y un conjunto de simulacion
        
    }
    
    private void tablaGenenal() {
        DefaultTableModel tcm = (DefaultTableModel) this.jTable9.getModel();

        
        for (Map.Entry<String, ArrayList<Company>> dataSimulation : mapAllSimlation.entrySet()){
            
            Object[] data = new Object[24];
            String claveSim = dataSimulation.getKey();
            ArrayList<Company> valueSim = dataSimulation.getValue();
            double contGan=0, contIns=0, promGan=0, promIns=0;
            for(Company company: valueSim) {
                contIns += company.getAdministration().getUnsatisfiedDemandWine().getPromedio();
                contGan += company.getAdministration().getWinningWines();
            }
            promGan = contGan/valueSim.size();
            promIns = contIns/valueSim.size();
            
            String[] dest = claveSim.split("-");
            data[0] = dest[0];
            data[1] = dest[1];
            data[2] = dest[2];
            data[3] = promGan;
            data[4] = promIns;
            tcm.addRow(data);
        }
    }
    
    private void loadTable(DefaultTableModel tcm, String destinacion, ArrayList<Company> lista ) {
        
        double cont = 1;
        for(Company comp: lista) {
            Object[] data = new Object[25];
            
            System.out.println(comp);
            
            // "Nro"
            data[0] = (int) cont;
            
            // "Hectareas" 
            data[1] = (int) comp.getVineyard().getHectaresOfGrapes();
            
            // "Krg por hectarea", 
            data[2] = (int) comp.getVineyard().getKilogramsByHectareOfGrape();
            
            // "sobreproduccion (%)", 
            data[3] = (int) (comp.getVineyard().getPercentageOfOverproduction()*100) + " %";
            
            // "producción total (kg)", 
            data[4] = (int) comp.getVineyard().getTotalKilogramsOfGrapes();
            
            // "uvas descartadas (%)", 
            data[5] = (int) (comp.getVineyard().getPercentageOfLostByTheSelectionProcess() * 100) + " %";
            
            // "uva despues de selección (kg)", 
            data[6] = (int) comp.getVineyard().getKilogramsOfGrapeAfterSelection();
            
            // "Vino fermentado (lt)", 
            data[7] = (int) comp.getVineyard().getLitersOfWineAfterFermantation();
            
            // "Perdida filtración(%)", 
            data[8] = (int) (comp.getVineyard().getPercentageOfLostByTheMacerationProcess() * 100) + " %";
            
            // "Vino Maceracion total (lt)", 
            data[9] = comp.getVineyard().getLitersOfWineAfterMaceration();
            
            // "Vino joven (lt)", 
            data[10] = comp.getVineyard().getLitersOfYoungWine();
            
            // "Vino añejo (lt)", 
            data[11] = comp.getVineyard().getLitersOfAgedWine();
            
            // "vino espumante (lt)", 
            data[12] = comp.getVineyard().getLitersOfSparklingWine();
            
            // "Demanda vino joven (lt)", 
            data[13] = comp.getAdministration().getDemandOfWine().getDemandYoung().getDemand();
            
            // "Demanda añejo (lt)", 
            data[14] = comp.getAdministration().getDemandOfWine().getDemandAged().getDemand();
            
            // "Demanda Espumante (lt)", 
            data[15] = comp.getAdministration().getDemandOfWine().getDemandSparkling().getDemand();
            
            // "\"GANANCIA\" Bs (Demanda x precio)", 
            data[16] = comp.getAdministration().getWinningWines();
            
            // "PERDIDA jovenes (bs)", 
            data[17] = comp.getAdministration().getLossOfWines().getLossOfWinesYoung();
            
            // "PERDIDA espumantes(Bs)", 
            data[18] = comp.getAdministration().getLossOfWines().getLossOfWinesSparkling();
            
            // "PERDIDA añejos(Bs)", 
            data[19] = comp.getAdministration().getLossOfWines().getLossOfWinesAge();
            
            // "DEMANDA INSATISFECHA jovenes", 
            data[20] = comp.getAdministration().getUnsatisfiedDemandWine().getUnsatisfiedDemandWinesYoung();
            
            // "DEMANDA INSATISFECHA espumantes",
            data[21] = comp.getAdministration().getUnsatisfiedDemandWine().getUnsatisfiedDemandWinesSparkling();
            
            // "DEMANDA INSATISFECHA añejos", 
            data[22] = comp.getAdministration().getUnsatisfiedDemandWine().getUnsatisfiedDemandWinesAge();
            
            // "DEMANDA INSATISFECHA promedio", 
            data[23] = comp.getAdministration().getUnsatisfiedDemandWine().getPromedio();
            
            
            // "Total utilidad(Bs)"
            data[24] = comp.getAdministration().getTotalUtility();
            
            tcm.addRow(data);
            
            cont++;
        }
        
        /*
        tcm.addRow(new Float[] {new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33) });
        tcm.addRow(new Float[] {new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33) });
        tcm.addRow(new Float[] {new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33) });
        tcm.addRow(new Float[] {new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33) });
        tcm.addRow(new Float[] {new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33) });
        */
    }
    
    private void loadData() {
        
        Object[][] data = new Object[][] {
            { new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33) },
            { new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33) },
            { new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33) },
            { new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33) },
            { new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33) },
        };
        
        DefaultTableModel tcm = (DefaultTableModel) this.jTable1.getModel();
        tcm.addRow(new Float[] {new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33) });
        tcm.addRow(data[0]);
        
        /*
        tcm.addRow(new Float[] {new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33) });
        tcm.addRow(new Float[] {new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33) });
        tcm.addRow(new Float[] {new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33) });
        tcm.addRow(new Float[] {new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33) });
        tcm.addRow(new Float[] {new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33) });
        tcm.addRow(new Float[] {new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33), new Float(12.33) });
        */
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane10 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jLabel15 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jScrollPane11 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jLabel20 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jScrollPane12 = new javax.swing.JScrollPane();
        jTable4 = new javax.swing.JTable();
        jLabel25 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jScrollPane13 = new javax.swing.JScrollPane();
        jTable5 = new javax.swing.JTable();
        jLabel30 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jScrollPane14 = new javax.swing.JScrollPane();
        jTable6 = new javax.swing.JTable();
        jLabel34 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jScrollPane15 = new javax.swing.JScrollPane();
        jTable7 = new javax.swing.JTable();
        jLabel35 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jTable8 = new javax.swing.JTable();
        jLabel36 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jLabel58 = new javax.swing.JLabel();
        jScrollPane16 = new javax.swing.JScrollPane();
        jTable9 = new javax.swing.JTable();
        jLabel10 = new javax.swing.JLabel();

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setPreferredSize(new java.awt.Dimension(1000, 700));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        jTable1.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nro", "Hectareas", "Krg por hectarea", "Sobreproduccion (%)", "Producción total (kg)", "Uvas descartadas (%) ", "Uva despues de selección (kg)", "Vino fermentado (lt)", "Perdida filtración(%)", "Vino Maceracion total (lt)", "Vino joven (lt)", "Vino añejo (lt)", "Vino espumante (lt)", "Demanda vino joven (lt)", "Demanda añejo (lt)", "Demanda Espumante", "\"GANANCIA\" Bs (Demanda x precio)", "Perdida jovenes (bs)", "Perdida espumantes(Bs)", "Perdida añejos(Bs)", "Demanda insatisfecha jovenes", "Demanda insatisfecha espumantes", "Demanda insatisfecha añejos", "demanda insatisfecha(promedio)", "Total utilidad(Bs)"
            }
        ));
        jTable1.setColumnSelectionAllowed(true);
        jTable1.setRowHeight(30);
        jScrollPane1.setViewportView(jTable1);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 120, 920, 520));

        jLabel1.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("10% jovenes");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 70, -1, -1));

        jLabel2.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("80% añejos");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 70, -1, -1));

        jLabel3.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("10% espumantes");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 70, -1, -1));

        jLabel4.setFont(new java.awt.Font("Dialog", 3, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("SIMULADOR DE PRODUCCION DE VINOS");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 20, 610, 40));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/frontis vino 2.jpg"))); // NOI18N
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 990, 670));

        jTabbedPane1.addTab("10-80-10", jPanel1);

        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setText("20% jovenes");
        jPanel6.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 80, -1, -1));

        jLabel7.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 0, 0));
        jLabel7.setText("70% añejos");
        jPanel6.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 80, -1, -1));

        jLabel8.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 0, 0));
        jLabel8.setText("10% espumantes");
        jPanel6.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 80, -1, -1));

        jLabel9.setFont(new java.awt.Font("Dialog", 3, 24)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 0, 0));
        jLabel9.setText("SIMULADOR DE PRODUCCION DE VINOS");
        jPanel6.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 20, 560, 40));

        jScrollPane10.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        jTable2.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nro", "Hectareas", "Krg por hectarea", "Sobreproduccion (%)", "Producción total (kg)", "Uvas descartadas (%) ", "Uva despues de selección (kg)", "Vino fermentado (lt)", "Perdida filtración(%)", "Vino Maceracion total (lt)", "Vino joven (lt)", "Vino añejo (lt)", "Vino espumante (lt)", "Demanda vino joven (lt)", "Demanda añejo (lt)", "Demanda Espumante", "\"GANANCIA \" Bs (Demanda x precio)", "Perdida jovenes (bs)", "Perdida espumantes(Bs)", "Perdida añejos(Bs)", "Demanda insatisfecha jovenes", "Demanda insatisfecha espumantes", "Demanda insatisfecha añejos", "demanda insatisfecha(promedio)", "Total utilidad(Bs)"
            }
        ));
        jTable2.setColumnSelectionAllowed(true);
        jTable2.setRowHeight(30);
        jScrollPane10.setViewportView(jTable2);

        jPanel6.add(jScrollPane10, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 120, 910, 520));

        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/frontis vino 2.jpg"))); // NOI18N
        jPanel6.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 990, 670));

        jTabbedPane1.addTab("20-70-10", jPanel6);

        jPanel7.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel11.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 0, 0));
        jLabel11.setText("30% jovenes");
        jPanel7.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(205, 78, -1, -1));

        jLabel12.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 0, 0));
        jLabel12.setText("60% añejos");
        jPanel7.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(408, 78, -1, -1));

        jLabel13.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(0, 0, 0));
        jLabel13.setText("10% espumantes");
        jPanel7.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(623, 78, -1, -1));

        jLabel14.setFont(new java.awt.Font("Dialog", 3, 24)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(0, 0, 0));
        jLabel14.setText("SIMULADOR DE PRODUCCION DE VINOS");
        jPanel7.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(218, 20, -1, 40));

        jScrollPane11.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        jTable3.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nro", "Hectareas", "Krg por hectarea", "Sobreproduccion (%)", "Producción total (kg)", "Uvas descartadas (%) ", "Uva despues de selección (kg)", "Vino fermentado (lt)", "Perdida filtración(%)", "Vino Maceracion total (lt)", "Vino joven (lt)", "Vino añejo (lt)", "Vino espumante (lt)", "Demanda vino joven (lt)", "Demanda añejo (lt)", "Demanda Espumante", "\"GANANCIA\"  Bs (Demanda x precio)", "Perdida jovenes (bs)", "Perdida espumantes(Bs)", "Perdida añejos(Bs)", "Demanda insatisfecha jovenes", "Demanda insatisfecha espumantes", "Demanda insatisfecha añejos", "demanda insatisfecha(promedio)", "Total utilidad(Bs)"
            }
        ));
        jTable3.setColumnSelectionAllowed(true);
        jTable3.setRowHeight(30);
        jScrollPane11.setViewportView(jTable3);

        jPanel7.add(jScrollPane11, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 122, 910, 520));

        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/frontis vino 2.jpg"))); // NOI18N
        jPanel7.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 670));

        jTabbedPane1.addTab("30-60-10", jPanel7);

        jPanel8.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel16.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(0, 0, 0));
        jLabel16.setText("40% jovenes");
        jPanel8.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(204, 87, -1, -1));

        jLabel17.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(0, 0, 0));
        jLabel17.setText("50% añejos");
        jPanel8.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(407, 87, -1, -1));

        jLabel18.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(0, 0, 0));
        jLabel18.setText("10% espumantes");
        jPanel8.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(622, 87, -1, -1));

        jLabel19.setFont(new java.awt.Font("Dialog", 3, 24)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(0, 0, 0));
        jLabel19.setText("SIMULADOR DE PRODUCCION DE VINOS");
        jPanel8.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(213, 29, 545, 40));

        jScrollPane12.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        jTable4.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jTable4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nro", "Hectareas", "Krg por hectarea", "Sobreproduccion (%)", "Producción total (kg)", "Uvas descartadas (%) ", "Uva despues de selección (kg)", "Vino fermentado (lt)", "Perdida filtración(%)", "Vino Maceracion total (lt)", "Vino joven (lt)", "Vino añejo (lt)", "Vino espumante (lt)", "Demanda vino joven (lt)", "Demanda añejo (lt)", "Demanda Espumante", "\"GANANCIA\"  Bs (Demanda x precio)", "Perdida jovenes (bs)", "Perdida espumantes(Bs)", "Perdida añejos(Bs)", "Demanda insatisfecha jovenes", "Demanda insatisfecha espumantes", "Demanda insatisfecha añejos", "demanda insatisfecha(promedio)", "Total utilidad(Bs)"
            }
        ));
        jTable4.setColumnSelectionAllowed(true);
        jTable4.setRowHeight(30);
        jScrollPane12.setViewportView(jTable4);

        jPanel8.add(jScrollPane12, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 122, 915, 517));

        jLabel25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/frontis vino 2.jpg"))); // NOI18N
        jPanel8.add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 670));

        jTabbedPane1.addTab("40-50-10", jPanel8);

        jPanel9.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel21.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(0, 0, 0));
        jLabel21.setText("50% jovenes");
        jPanel9.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(206, 87, -1, -1));

        jLabel22.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(0, 0, 0));
        jLabel22.setText("40% añejos");
        jPanel9.add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(409, 87, -1, -1));

        jLabel23.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(0, 0, 0));
        jLabel23.setText("10% espumantes");
        jPanel9.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(624, 87, -1, -1));

        jLabel24.setFont(new java.awt.Font("Dialog", 3, 24)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(0, 0, 0));
        jLabel24.setText("SIMULADOR DE PRODUCCION DE VINOS");
        jPanel9.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(221, 29, 549, 40));

        jScrollPane13.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        jTable5.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jTable5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nro", "Hectareas", "Krg por hectarea", "Sobreproduccion (%)", "Producción total (kg)", "Uvas descartadas (%) ", "Uva despues de selección (kg)", "Vino fermentado (lt)", "Perdida filtración(%)", "Vino Maceracion total (lt)", "Vino joven (lt)", "Vino añejo (lt)", "Vino espumante (lt)", "Demanda vino joven (lt)", "Demanda añejo (lt)", "Demanda Espumante", "\"GANANCIA\"  Bs (Demanda x precio)", "Perdida jovenes (bs)", "Perdida espumantes(Bs)", "Perdida añejos(Bs)", "Demanda insatisfecha jovenes", "Demanda insatisfecha espumantes", "Demanda insatisfecha añejos", "demanda insatisfecha(promedio)", "Total utilidad(Bs)"
            }
        ));
        jTable5.setColumnSelectionAllowed(true);
        jTable5.setRowHeight(30);
        jScrollPane13.setViewportView(jTable5);

        jPanel9.add(jScrollPane13, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 122, 910, 518));

        jLabel30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/frontis vino 2.jpg"))); // NOI18N
        jPanel9.add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 680));

        jTabbedPane1.addTab("50-40-10", jPanel9);

        jPanel10.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel26.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(0, 0, 0));
        jLabel26.setText("60% jovenes");
        jPanel10.add(jLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(203, 87, -1, -1));

        jLabel27.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(0, 0, 0));
        jLabel27.setText("30% añejos");
        jPanel10.add(jLabel27, new org.netbeans.lib.awtextra.AbsoluteConstraints(406, 87, -1, -1));

        jLabel28.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(0, 0, 0));
        jLabel28.setText("10% espumantes");
        jPanel10.add(jLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(621, 87, -1, -1));

        jLabel29.setFont(new java.awt.Font("Dialog", 3, 24)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(0, 0, 0));
        jLabel29.setText("SIMULADOR DE PRODUCCION DE VINOS");
        jPanel10.add(jLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(215, 29, -1, 40));

        jScrollPane14.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        jTable6.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jTable6.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nro", "Hectareas", "Krg por hectarea", "Sobreproduccion (%)", "Producción total (kg)", "Uvas descartadas (%) ", "Uva despues de selección (kg)", "Vino fermentado (lt)", "Perdida filtración(%)", "Vino Maceracion total (lt)", "Vino joven (lt)", "Vino añejo (lt)", "Vino espumante (lt)", "Demanda vino joven (lt)", "Demanda añejo (lt)", "Demanda Espumante", "\"GANANCIA\"   Bs (Demanda x precio)", "Perdida jovenes (bs)", "Perdida espumantes(Bs)", "Perdida añejos(Bs)", "Demanda insatisfecha jovenes", "Demanda insatisfecha espumantes", "Demanda insatisfecha añejos", "demanda insatisfecha(promedio)", "Total utilidad(Bs)"
            }
        ));
        jTable6.setColumnSelectionAllowed(true);
        jTable6.setRowHeight(30);
        jScrollPane14.setViewportView(jTable6);

        jPanel10.add(jScrollPane14, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 122, 910, 520));

        jLabel34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/frontis vino 2.jpg"))); // NOI18N
        jPanel10.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 680));

        jTabbedPane1.addTab("60-30-10", jPanel10);

        jPanel11.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel31.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel31.setForeground(new java.awt.Color(0, 0, 0));
        jLabel31.setText("70% jovenes");
        jPanel11.add(jLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(211, 93, -1, -1));

        jLabel32.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel32.setForeground(new java.awt.Color(0, 0, 0));
        jLabel32.setText("20% añejos");
        jPanel11.add(jLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(414, 93, -1, -1));

        jLabel33.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel33.setForeground(new java.awt.Color(0, 0, 0));
        jLabel33.setText("10% espumantes");
        jPanel11.add(jLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(629, 93, -1, -1));

        jLabel49.setFont(new java.awt.Font("Dialog", 3, 24)); // NOI18N
        jLabel49.setForeground(new java.awt.Color(0, 0, 0));
        jLabel49.setText("SIMULADOR DE PRODUCCION DE VINOS");
        jPanel11.add(jLabel49, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 25, 545, 40));

        jScrollPane15.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        jTable7.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jTable7.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nro", "Hectareas", "Krg por hectarea", "Sobreproduccion (%)", "Producción total (kg)", "Uvas descartadas (%) ", "Uva despues de selección (kg)", "Vino fermentado (lt)", "Perdida filtración(%)", "Vino Maceracion total (lt)", "Vino joven (lt)", "Vino añejo (lt)", "Vino espumante (lt)", "Demanda vino joven (lt)", "Demanda añejo (lt)", "Demanda Espumante", "\"GANANCIA\"  Bs (Demanda x precio)", "Perdida jovenes (bs)", "Perdida espumantes(Bs)", "Perdida añejos(Bs)", "Demanda insatisfecha jovenes", "Demanda insatisfecha espumantes", "Demanda insatisfecha añejos", "demanda insatisfecha(promedio)", "Total utilidad(Bs)"
            }
        ));
        jTable7.setColumnSelectionAllowed(true);
        jTable7.setRowHeight(30);
        jScrollPane15.setViewportView(jTable7);

        jPanel11.add(jScrollPane15, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 122, 918, 520));

        jLabel35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/frontis vino 2.jpg"))); // NOI18N
        jPanel11.add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 680));

        jTabbedPane1.addTab("70-20-10", jPanel11);

        jPanel12.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel51.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel51.setForeground(new java.awt.Color(0, 0, 0));
        jLabel51.setText("80% jovenes");
        jPanel12.add(jLabel51, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 81, -1, -1));

        jLabel52.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel52.setForeground(new java.awt.Color(0, 0, 0));
        jLabel52.setText("10% añejos");
        jPanel12.add(jLabel52, new org.netbeans.lib.awtextra.AbsoluteConstraints(413, 81, -1, -1));

        jLabel53.setFont(new java.awt.Font("Dialog", 3, 14)); // NOI18N
        jLabel53.setForeground(new java.awt.Color(0, 0, 0));
        jLabel53.setText("10% espumantes");
        jPanel12.add(jLabel53, new org.netbeans.lib.awtextra.AbsoluteConstraints(628, 81, -1, -1));

        jLabel54.setFont(new java.awt.Font("Dialog", 3, 24)); // NOI18N
        jLabel54.setForeground(new java.awt.Color(0, 0, 0));
        jLabel54.setText("SIMULADOR DE PRODUCCION DE VINOS");
        jPanel12.add(jLabel54, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 23, 562, 40));

        jScrollPane9.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        jTable8.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jTable8.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nro", "Hectareas", "Krg por hectarea", "Sobreproduccion (%)", "Producción total (kg)", "Uvas descartadas (%) ", "Uva despues de selección (kg)", "Vino fermentado (lt)", "Perdida filtración(%)", "Vino Maceracion total (lt)", "Vino joven (lt)", "Vino añejo (lt)", "Vino espumante (lt)", "Demanda vino joven (lt)", "Demanda añejo (lt)", "Demanda Espumante", "\"GANANCIA\"   Bs (Demanda x precio)", "Perdida jovenes (bs)", "Perdida espumantes(Bs)", "Perdida añejos(Bs)", "Demanda insatisfecha jovenes", "Demanda insatisfecha espumantes", "Demanda insatisfecha añejos", "demanda insatisfecha(promedio)", "Total utilidad(Bs)"
            }
        ));
        jTable8.setColumnSelectionAllowed(true);
        jTable8.setRowHeight(30);
        jScrollPane9.setViewportView(jTable8);

        jPanel12.add(jScrollPane9, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 122, 907, 514));

        jLabel36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/frontis vino 2.jpg"))); // NOI18N
        jPanel12.add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 680));

        jTabbedPane1.addTab("80-10-10", jPanel12);

        jPanel13.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel58.setFont(new java.awt.Font("Dialog", 3, 24)); // NOI18N
        jLabel58.setForeground(new java.awt.Color(0, 0, 0));
        jLabel58.setText("SIMULADOR DE PRODUCCION DE VINOS");
        jPanel13.add(jLabel58, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 17, -1, 40));

        jScrollPane16.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        jTable9.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jTable9.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Jovenes", "Añejos", "Espumantes", "Ganancia", "Demanda insatisfecha"
            }
        ));
        jTable9.setColumnSelectionAllowed(true);
        jTable9.setRowHeight(30);
        jScrollPane16.setViewportView(jTable9);

        jPanel13.add(jScrollPane16, new org.netbeans.lib.awtextra.AbsoluteConstraints(189, 118, 592, 209));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/frontis vino 2.jpg"))); // NOI18N
        jPanel13.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 990, 670));

        jTabbedPane1.addTab("tabla-resumen", jPanel13);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 707, Short.MAX_VALUE)
        );

        jTabbedPane1.getAccessibleContext().setAccessibleName("20-70-10");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane16;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTable jTable4;
    private javax.swing.JTable jTable5;
    private javax.swing.JTable jTable6;
    private javax.swing.JTable jTable7;
    private javax.swing.JTable jTable8;
    private javax.swing.JTable jTable9;
    // End of variables declaration//GEN-END:variables
}
