/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.classes.Company;
import controller.dto.CompanyDTO;
import java.util.Map;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;


public class Graficas extends javax.swing.JDialog {

    private Company bestCompany;
    private Map<Double, Map<String, Company>> mapCompany;
    
    public Graficas(java.awt.Frame parent, boolean modal, Company bestCompany, Map<Double, Map<String, Company>> mapCompany) {
        super(parent, modal);
        this.bestCompany = bestCompany;
        this.mapCompany = mapCompany;
        initComponents();
    }
    
    private void initComponents2() {
        this.jPanel1.setOpaque(false);
    }
    
    private void graficarGanancia(){
        DefaultCategoryDataset dtsc = new DefaultCategoryDataset();
        // Map<Double, Map<String, Company>> mapCompany
        
        for ( Map.Entry<Double, Map<String, Company>> datacompany : this.mapCompany.entrySet()){
            Double clave = datacompany.getKey();
            
            Company companyAux = (Company) this.mapCompany.get(clave).get("best");
            int aniejos = (int) (companyAux.getVineyard().getPercentageDestinedForAgedWines() * 100);
            int jovenes = (int) (companyAux.getVineyard().getPercentageDestinedForYoungWines() * 100);
            int espumantes = (int) (companyAux.getVineyard().getPercentageDestinedForSparklingWines() * 100);
            System.out.println(aniejos);
            String destination = jovenes + "-"+ aniejos + "-"+espumantes;
            dtsc.setValue(clave, destination, "");
        }
        
        
        JFreeChart ch = ChartFactory.createBarChart3D("grafica de Ganancias", "destinacion de vinos", "ganancia", dtsc, PlotOrientation.VERTICAL, true, true, false);
        ChartPanel cp = new ChartPanel(ch);
        this.jPanel1.add(cp);
        cp.setBounds(0, 0, 750, 450);
    }
    
    private void graficarDestinacion(){
        DefaultCategoryDataset dtsc = new DefaultCategoryDataset();
        
        int aniejos = (int) (this.bestCompany.getVineyard().getPercentageDestinedForAgedWines() * 100);
        int jovenes = (int) (this.bestCompany.getVineyard().getPercentageDestinedForYoungWines() * 100);
        int espumantes = (int) (this.bestCompany.getVineyard().getPercentageDestinedForSparklingWines() * 100);
                
        dtsc.setValue(jovenes, "jovenes", "");
        dtsc.setValue(aniejos, "añejos", "");
        dtsc.setValue(espumantes, "espumantes", "");
        
        JFreeChart ch = ChartFactory.createBarChart3D("destinacion optima de vinos", "destinacion de vinos", "porcentaje", dtsc, PlotOrientation.HORIZONTAL, true, true, false);
        ChartPanel cp = new ChartPanel(ch);
        this.jPanel1.add(cp);
        cp.setBounds(0, 0, 750, 450);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        back = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(600, 400));
        setMinimumSize(new java.awt.Dimension(600, 400));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 756, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 472, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 80, -1, -1));

        jButton1.setText("GANANCIA");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 100, 130, 41));

        back.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/atras_v.jpeg"))); // NOI18N
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });
        getContentPane().add(back, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 330, 150, 50));

        jLabel1.setFont(new java.awt.Font("Dialog", 3, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("SIMULADOR DE PRODUCCION DE VINOS");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 20, 550, 40));

        jButton2.setText("DESTINACION");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 220, 130, 42));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/frontis vino 2.jpg"))); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 710));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_backActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        graficarGanancia();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        graficarDestinacion();
    }//GEN-LAST:event_jButton2ActionPerformed

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton back;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
